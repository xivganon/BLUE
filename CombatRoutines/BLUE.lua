--- Blue Mage ACR
-- @module BLUE
local BLUE = {}
if not _G["aa"] then
	return nil
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                         Internal Info                               --
-- -------------------------------------------------------------------------- --

BLUE._VERSION = "0.1.0"
BLUE._NAME = "Blue"
BLUE._DESCRIPTION = "Blue Mage ACR"
BLUE._URL = ""
BLUE._ID = ""

d("[AnonAddons] Blue Loaded")
-- io.popen('cmd /c copy /y '..GetLuaModsPath()..[[AABlue\BLUE.lua]]..' '..GetLuaModsPath()..[[ACR\CombatRoutines\Blue.lua]])
-- -------------------------------------------------------------------------- --
-- ANCHOR                       Module variables                              --
-- -------------------------------------------------------------------------- --

BLUE.GUI = {
    windowOpen = false,
    windowVisible = false,
    toggleVisible = true,
    name = BLUE._NAME
}

BLUE.isPVP = false
BLUE.isPVE = true

BLUE.classes = {
    [FFXIV.JOBS.BLUEMAGE] = true,
}

BLUE.state = {
    lastEnemyCast = -1,
    queue = {},
}

BLUE.settings = {
    filter = 1, -- BLUE.DAMAGEFILTER.NONE
    interrupts = true,
}

BLUE.GCD = {

}

BLUE.actions = {

}
-- astral 2121 
-- umbral 2122

-- 23113 mimicked impending doom
-- -------------------------------------------------------------------------- --
-- ANCHOR                        Initialization                               --
-- -------------------------------------------------------------------------- --

function BLUE.OnLoad()
    BLUE.LoadActions()

    aa["BLUE"] = {
		GetInfo = function()
			return {
				Version = BLUE._VERSION,
				Name = BLUE._NAME,
				Description = BLUE._DESCRIPTION,
				Url = BLUE._URL,
				Id = BLUE._ID,
			}
        end,
        SetPhys = function(b)
            BLUE.settings.physicalOnly = b
        end,
        GetPhys = function()
            return BLUE.settings.physicalOnly
        end,
        SetMagi = function(b)
            BLUE.settings.magicOnly = b
        end,
        GetMagi = function()
            return BLUE.settings.magicOnly
        end,
        SetInter = function(b)
            BLUE.settings.interrupts = b
        end,
        GetInter = function()
            return BLUE.settings.interrupts
        end,
	}
end

function BLUE.LoadActions()
    BLUE.actions = {}
    local acList = ActionList:Get(1)
    for actionId, action in pairs(acList) do
        if (action.job == Player.job or action.job == 1) and action.usable == true then
            BLUE.actions[action.name] = action
        end
    end
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                           Rendering                                 --
-- -------------------------------------------------------------------------- --

function BLUE.OnOpen()
    BLUE.GUI.windowOpen = true
end

function BLUE.Draw()
    if BLUE.GUI.WindowOpen then
        GUI:SetNextWindowPosCenter(GUI.SetCond_FirstUseEver)
        GUI:SetNextWindowSize(0,0,GUI.SetCond_Always)
        BLUE.GUI.windowVisible, BLUE.GUI.windowOpen = GUI:Begin(
            'Blue##BLUEAcrOptions',
            BLUE.GUI.WindowOpen,
            GUI.WindowFlags_NoResize
        )
            if BLUE.GUI.windowVisible then

            end
        GUI:End()
    end
    GUI:SetNextWindowPosCenter(GUI.SetCond_FirstUseEver)
    GUI:SetNextWindowSize(0,0,GUI.SetCond_Always)
    GUI:Begin('BlueOverlay##BLUEOverlay', true, GUI.WindowFlags_NoResize + GUI.WindowFlags_NoTitleBar)

        if BLUE.IsTank() then
            GUI:TextColored(0.1,0.1,0.9,1, 'TANK ROLE')
        elseif BLUE.IsHealer() then
            GUI:TextColored(0.1,0.9,0.1,1, 'HEALER ROLE')
        elseif BLUE.IsDPS() then
            GUI:TextColored(0.1,0.9,0.1,1, 'DPS ROLE')
        elseif BLUE.MissingMimic() then
            GUI:TextColored(0.9,0.9,0.9,1, 'NO MIMIC')
        end

        if GUI:Button('Reload Actions##BLUEReload') then
            BLUE.LoadActions()
        end

        if BLUE.settings.filter == BLUE.DAMAGEFILTER.MAGIC then
            aa.PushStyle(aa.Layouts.toggleOn)
            if GUI:Button('Magic Only##BLUEMagic') then
                BLUE.settings.filter = BLUE.DAMAGEFILTER.NONE
            end
            aa.PopStyle(aa.Layouts.toggleOn)
        else
            aa.PushStyle(aa.Layouts.toggleOff)
            if GUI:Button('Magic Only##BLUEMagic') then
                BLUE.settings.filter = BLUE.DAMAGEFILTER.MAGIC
            end
            aa.PopStyle(aa.Layouts.toggleOff)
        end
        if BLUE.settings.filter == BLUE.DAMAGEFILTER.PHYSICAL  then
            aa.PushStyle(aa.Layouts.toggleOn)
            if GUI:Button('Physical Only##BLUEPhysical') then
                BLUE.settings.filter = BLUE.DAMAGEFILTER.NONE
            end
            aa.PopStyle(aa.Layouts.toggleOn)
        else
            aa.PushStyle(aa.Layouts.toggleOff)
            if GUI:Button('Physical Only##BLUEPhysical') then
                BLUE.settings.filter = BLUE.DAMAGEFILTER.PHYSICAL
            end
            aa.PopStyle(aa.Layouts.toggleOff)
        end
        if BLUE.actions['Flying Sardine'] then
            if BLUE.settings.interrupts then
                aa.PushStyle(aa.Layouts.toggleOn)
                if GUI:Button('Interrupts##BLUEInterrupts') then
                    BLUE.settings.interrupts = not BLUE.settings.interrupts
                end
                aa.PopStyle(aa.Layouts.toggleOn)
            else
                aa.PushStyle(aa.Layouts.toggleOff)
                if GUI:Button('Interrupts##BLUEInterrupts') then
                    BLUE.settings.interrupts = not BLUE.settings.interrupts
                end
                aa.PopStyle(aa.Layouts.toggleOff)
            end
        else
            aa.PushStyle(aa.Layouts.toggleDisabled)
            GUI:Button('Interrupts##BLUEInterrupts')
            aa.PopStyle(aa.Layouts.toggleDisabled)
        end
        
        -- for k,v in pairs(BLUE.actions) do
        --     GUI:Text(string.format('%20s',k)..
        --         '[ ID: '..string.format('%5s',v.id)..
        --         '| MP: '..string.format('%4s',v.cost)..
        --         '| CD: '..string.format('%-5s',math.floor(math.round(v.recasttime,4)*100)/100)..
        --         '| CT: '..string.format('%-5s',math.floor(math.round(v.casttime,4)*100)/100)..
        --         '| Cat:'..v.category..']')
        -- end
        if Player:GetTarget() then
            GUI:Separator()
            GUI:Text(Player:GetTarget().name)
            local angle = BLUE.AngleToTarget(Player:GetTarget()) 
            GUI:Text('Angle to target: '.. angle)
            if angle >= 0 and angle < math.pi * 0.25 then
                GUI:Text('Front')
            elseif angle >= math.pi * 0.25 and angle < math.pi * 0.75 then
                GUI:Text('Side')
            else
                GUI:Text('Back')
            end
            GUI:Text('INVULNERABLE: ' .. tostring(HasBuffs(target, BLUE.ENEMYINVULNERABLEBUFFS)) )
        end
        if BLUE.lastCast then
            GUI:Separator()
            GUI:Text(BLUE.lastCast.name)
        end
        GUI:Separator()
        GUI:Text("Queue:")
        if table.valid(BLUE.GCD) then
            for index, actionList in pairs(BLUE.GCD) do
                GUI:Text(index.."------------------------------")
                for position, action in pairs(actionList) do
                    GUI:Text(action.name)
                end
            end
        end
        GUI:Separator()
        GUI:Separator()
    GUI:End()
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                             Logic                                   --
-- -------------------------------------------------------------------------- --

function BLUE.OnUpdate(event, ticks)
    if not Player.incombat then
        BLUE.lastCast = nil
    else
        BLUE.lastCast = table.valid(Player.castinginfo) and ActionList:Get(1, Player.castinginfo.lastcastid) or nil
    end
    local target = Player:GetTarget()
    if target then
        if BLUE.IsTimeToStopActing(target) and ActionList:IsCasting() then
            ActionList:StopCasting()
        end
        if not table.valid(BLUE.GCD)then
            local newGCD = BLUE.BestGCD(BLUE.settings.filter)
            if table.valid(newGCD) then
                BLUE.GCD[1] = newGCD
            end
        end
    end
    if Player:IsMoving() then
        BLUE.GCD = {}
    end
end

function BLUE:Cast()
    local target = Player:GetTarget()
    local shouldInterrupt = false;
    if target and target.attackable then
        if BLUE.IsTimeToStopActing(target) then 
        else
            shouldInterrupt = target.castinginfo.castinginterruptible and BLUE.settings.interrupts and BLUE.actions['Flying Sardine'] and Player.castinginfo.lastcastid ~= BLUE.actions['Flying Sardine'].id
            if table.valid(BLUE.GCD) and target.attackable and (Player.incombat or gStartCombat)
            and ((not ActionList:IsCasting())) then
                if assert(BLUE.NextAction()) then
                    if BLUE.NextAction().category == 4 then
                        if Player.castinginfo.lastcastid == BLUE.NextAction().id then
                            BLUE.PopAction()
                            if not table.valid(BLUE.GCD)then
                                local newGCD = BLUE.BestGCD(BLUE.settings.filter, shouldInterrupt)
                                if newGCD then
                                    BLUE.GCD[1] = newGCD
                                end
                            end
                        end
                    end
                    if BLUE.NextAction().category ~= 4 then
                        if Player.castinginfo.casttime == 0 and Player.castinginfo.lastcastid == BLUE.NextAction().id then
                            BLUE.PopAction()
                            if not table.valid(BLUE.GCD)then
                                local newGCD = BLUE.BestGCD(BLUE.settings.filter, shouldInterrupt)
                                if newGCD then
                                    BLUE.GCD[1] = newGCD
                                end
                            end
                        end
                    end
                    if BLUE.NextAction():IsReady(target.id)  then
                        BLUE.NextAction():Cast(target.id)
                        d("Casting "..BLUE.NextAction().name)
                        return true
                    elseif BLUE.NextAction():IsReady() then
                        BLUE.NextAction():Cast()
                        d("Casting "..BLUE.NextAction().name)
                        return true
                    end
                end
            end
        end
    end
    return false
end

function BLUE.BestGCD(filter, shouldInterrupt)
    local target = Player:GetTarget()
    if not target then return nil end
    local GCD = {}
    local bestFullGCD = nil
    local bestDirectFullGCD = nil
    local bestOGCD = nil
    local bestShortGCD = nil
    -- local secondFullGCD = nil
    -- local secondDirectFullGCD = nil
    -- local secondOGCD = nil
    -- local secondShortGCD = nil
    for name, action in pairs(BLUE.actions) do
        if BLUE.actionDB[action.name] then
            if BLUE.MatchFilter(action, filter, target) then
                if action.category == 2 and action.casttime > 1 then
                    if BLUE.ShouldCast(action) then
                        if not bestFullGCD then
                            if BLUE.ActionPotency(action) > 0 then
                                bestFullGCD = action
                            end
                        else
                            if BLUE.ActionPotency(action) > BLUE.ActionPotency(bestFullGCD) then
                                bestFullGCD = action
                            end
                        end
                    end
                elseif action.category == 2 and action.casttime <= 1 then
                    if BLUE.ShouldCast(action) then
                        if not bestShortGCD then
                            if BLUE.ActionPotency(action) > 0 then
                                bestShortGCD = action
                            end
                        else
                            if BLUE.ActionPotency(action) > BLUE.ActionPotency(bestShortGCD) then
                                bestShortGCD = action
                            end
                        end
                    end
                elseif action.category == 4 then
                    if BLUE.ShouldCast(action) then
                        if not bestOGCD then
                            if BLUE.ActionPotency(action) > 0 then
                                bestOGCD = action
                            end
                        else
                            if BLUE.ActionPotency(action) > BLUE.ActionPotency(bestOGCD) then
                                bestOGCD = action
                            end
                        end
                    end
                end
            end
        end
    end
    -- d('Found bests')
    -- d('Full GCD')
    -- if bestFullGCD then d(bestFullGCD.name) end
    -- d('Short GCD')
    -- if bestShortGCD then d(bestShortGCD.name) end
    -- d('OGCD')
    -- if bestOGCD then d(bestOGCD.name) end

    if shouldInterrupt then
        GCD = {BLUE.actions['Flying Sardine'], bestOGCD}
    elseif BLUE.ActionPotency(bestShortGCD)
     + BLUE.ActionPotency(bestOGCD) 
     > BLUE.ActionPotency(bestFullGCD) then
        GCD = {bestShortGCD, bestOGCD}
    else
        GCD = {bestFullGCD}
    end
    if table.valid(GCD) then
        return GCD
    else
        return nil
    end
end

function BLUE.IsImmune(entity)

end

function BLUE.IsTank()
    return HasBuff(Player, 2124)
end
function BLUE.IsDPS()
    return HasBuff(Player, 2125)
end
function BLUE.IsHealer()
    return HasBuff(Player, 2126)
end
function BLUE.MissingMimic()
    return not (BLUE.IsTank() or BLUE.IsDPS() or BLUE.IsHealer())
end

function BLUE.IsMagic(action)
    if action.attacktype == BLUE.TYPE.MAGIC then
        return true
     end
     return false
end
function BLUE.IsPhysical(action)
    if action.attacktype >= BLUE.TYPE.SLASH and action.attacktype <= BLUE.TYPE.BLUNT then
       return true 
    end
    return false
end

function BLUE.IsDot(action)
    if action then
        if BLUE.actionDB[action.name] then
            if BLUE.actionDB[action.name].dpotency and BLUE.actionDB[action.name].duration then
                return BLUE.actionDB[action.name].tbuff
            end
        end
    end
    return 0
end

function BLUE.ActionPotency(action)
    -- d('Calculating potency')
    local target = nil
    if Player:GetTarget() then
        target = Player:GetTarget()
    end
    if action then
        local aspect = BLUE.ASPECTS[BLUE.WEAKNESSES[target.iconid]]
        local dbAction = BLUE.actionDB[action.name]
        if dbAction then
            local dP, dotP, duration
            dP = (BLUE.actionDB[action.name]['potency'] or 0)
            dotP = (BLUE.actionDB[action.name]['dpotency'] or 0)
            duration = (BLUE.actionDB[action.name]['duration'] or 0)
            if dbAction.modifier then
                if dbAction.modifier.condition() then
                    if dbAction.modifier.potency then
                        dP = dbAction.modifier.potency
                    end
                    if dbAction.modifier.dpotency then
                        dotP = dbAction.modifier.dpotency
                    end
                    if dbAction.modifier.duration then
                        duration = dbAction.modifier.duration
                    end
                end
            end
            return (dP + dotP * duration) * (aspect == action.aspect and 2 or 1)
        end
    end
    return 0
end

function BLUE.QueueLength()
    local duration = 0
    for iGCD, GCD in pairs(BLUE.GCD) do
        duration = duration + math.floor(math.round(ActionList:Get(1, 6274).recasttime,4)*100)/100
    end
    return duration
end

function BLUE.NextAction()
    if table.valid(BLUE.GCD) then
        local current = BLUE.GCD[table.size(BLUE.GCD)]
        if table.valid(current) then
            return current[next(current)]
        end
    end
    return nil
end

function BLUE.PopAction()
    if table.valid(BLUE.GCD) then
        local current = BLUE.GCD[table.size(BLUE.GCD)]
        if table.valid(current) then
            table.remove(current, next(current))
            if not table.valid(current) then
                table.remove(BLUE.GCD, table.size(BLUE.GCD))
            end
        end
    end
end

function BLUE.IsOnGCD(action, isdot)
    local result = false
    local checkDot = isdot or false
    if table.valid(BLUE.GCD) then
        for index, act in pairs(BLUE.GCD[1]) do
            if act.id == action.id then
                result = true
            end
            if checkDot then
                if BLUE.actionDB[action.name] then

                end
            end
        end
    end
    if table.valid(Player.castinginfo) then
        if Player.castinginfo.lastcastid == action.id or Player.castinginfo.castingid == action.id or Player.castinginfo.channelingid == action.id then
            result = true
        end
    end
    return result
end

function BLUE.ShouldCast(action)
    local target = Player:GetTarget()
    if BLUE.IsDot(action) > 0 then
        if BLUE.IsOnGCD(action, true) then
            return false
        end
        if target then
            if HasBuff(target.id, BLUE.IsDot(action), nil, 3, Player.id) then
                return false
            end
        end
    end
    if action.recasttime > 3 and BLUE.IsOnGCD(action) then
        return false
    end
    if action.isoncd and action.cdmax > 3 then
        return false
    end

    if not BLUE.IsOnRange(action, target) then
        return false
    end

    return true
end

function BLUE.IsOnRange(action, entity) 
    local adbData = BLUE.actionDB[action.name]
    local aoeSwitch = {
        [0] = function() 
            local inRange = action.range > entity.distance2d - entity.hitradius
            return inRange
        end,
        [1] = function()
            local angle = BLUE.AngleToTarget(entity)
            local inFront = angle >= 0 and angle < math.pi * 0.25
            local inRadius = action.radius > entity.distance2d - entity.hitradius
            return inFront and inRadius
        end,
        [2] = function()
            local inRadius = action.radius > entity.distance2d - entity.hitradius
            return inRadius
        end,
        [3] = function()
            local inRadius = action.radius > entity.distance2d - entity.hitradius
            and adbData.mindistance < entity.distance2d - entity.hitradius
            return inRadius
        end,
        [4] = function()
            local angle = BLUE.AngleToTarget(entity)
            local inFront = angle >= 0 and angle < math.pi * 0.5
            local inRadius = action.radius > entity.distance2d - entity.hitradius
            return inFront and inRadius
        end,
        [5] = function()
            local inRadius = action.radius > entity.distance2d - entity.hitradius
            return inRadius
        end,
        [6] = function()
            local inRange = action.range > entity.distance2d - entity.hitradius
            return inRange
        end,
        [7] = function()
            local inRange = action.range > entity.distance2d - entity.hitradius
            return inRange
        end,
    }
    return aoeSwitch[IsNull(adbData.aeotype, 0)]()
end

function BLUE.MatchFilter(action, filter, enemy)
    local match = true
    local filterSwitch = {
        [1] = function() 
            return true
        end,
        [2] = function()
            if (not action.attacktype == 5) then
                return false
            end
            return true
        end,
        [3] = function()
            if action.attacktype == 5 then
                return false
            end
            return true
        end
    }
    match = filterSwitch[filter]()

    if BLUE.RESISTS[enemyId] then
        if action.attacktype > 0 then
            if BLUE.RESISTS[enemyId].type == action.attacktype then
                match = false
            end
        end
        if action.aspect > 0 then
            if BLUE.RESISTS[enemyId].aspect == action.aspect then
                match = false
            end
        end
    end

    return match
end

function BLUE.IsTimeToStopActing(target)
    local invBuff = HasBuffs(target, BLUE.ENEMYINVULNERABLEBUFFS)
    local pyretic = HasBuffs(Player, "")
    -- 23097 mimic
    local impSit = false;

    if (target.castinginfo.channelingid == 23097) then
        BLUE.state.lastEnemyCast = target.castinginfo.channelingid;
        if ActionList:IsCasting() then
            local pRem = Player.castinginfo.casttime - Player.castinginfo.channeltime
            local tRem = target.castinginfo.casttime - target.castinginfo.channeltime
            impSit = pRem >= tRem
        elseif table.valid(BLUE.GCD[1]) then
            impSit = BLUE.NextAction().casttime >= target.castinginfo.casttime - target.castinginfo.channeltime
        end
    elseif BLUE.state.lastEnemyCast > 0 then
        impSit = true
    end

    if (HasBuff(target, 2450) and BLUE.state.lastEnemyCast >= 0) then 
        BLUE.state.lastEnemyCast = -1
    end

    

    return invBuff or pyretic or impSit
end

function BLUE.AngleToTarget(entity)
	local playerHeading = ConvertHeading(Player.pos.h)
	local playerAngle = math.atan2(entity.pos.x - Player.pos.x, entity.pos.z - Player.pos.z) 
	local deviation = playerAngle - playerHeading

	return math.abs(deviation)
end

-- -------------------------------------------------------------------------- --
-- ANCHOR                           Database                                  --
-- -------------------------------------------------------------------------- --

BLUE.DAMAGEFILTER = {
    NONE = 1,
    MAGIC = 2,
    PHYSICAL = 3,
}

BLUE.TYPE = {
    SLASH = 1,
    PIERCING = 2,
    BLUNT = 3,
    MAGIC = 5,
}

BLUE.ASPECTS = {
    FIRE = 1,
    ICE = 2,
    WIND = 3,
    EARTH = 4,
    LIGHTNING = 5,
    WATER = 6,
    PHYSICAL = 7,
}

BLUE.AOETYPE = {
    CONE = 1,
    LINE = 2,
    DONUT = 3,
    HALFCIRCLE = 4,
    SAOE = 5,
    TAOE = 6,
    GAOE = 7,
}

BLUE.ENEMYINVULNERABLEBUFFS =  "2450" -- Mimic

BLUE.WEAKNESSES = {
    [60692] = 'WIND',
    [60693] = 'FIRE',
    [60694] = 'ICE',
    [60695] = 'WATER',
    [60696] = 'EARTH',
    [60697] = 'LIGHTNING',
}

-- {type = number, aspect = number}
BLUE.RESISTS = {
    [8077] = {type = BLUE.TYPE.SLASH, aspect = BLUE.ASPECTS.PHYSICAL},
    [8076] = {type = BLUE.TYPE.BLUNT, aspect = BLUE.ASPECTS.PHYSICAL},
}

BLUE.actionDB = {
    ["Water Cannon"] = {
        potency = 200,
    },
    ["Flamethrower"] = {
        potency = 220,
        falloff = true,
        aoetype = BLUE.AOETYPE.CONE
    },
    ["Aqua Breath"] = {
        potency = 140,
        falloff = true,
        tbuff = 1736,
        duration = 12,
        dpotency = 20,
        aoetype = BLUE.AOETYPE.CONE
    },
    ["Drill Cannons"] = {
        potency = 200,
        falloff = true,
        modifier = {
            condition = function()
            end,
            potency = 600,
            falloff = true,
        },
        aoetype = BLUE.AOETYPE.LINE
    },
    ["High Voltage"] = {
        potency = 180,
        falloff = true,
        tbuff = 17,
        duration = 15,
        modifier = {
            condition = function()
                return HasBuff(Player:GetTarget(), 1736)
            end,
            potency = 220,
            duration = 30,
        },
        aoetype = BLUE.AOETYPE.CONE
    },
    ["Loom"] = {},
    ["Final Sting"] = {
        potency = 2000,
        suicide = true,
    },
    ["Song of Torment"] = {
        dpotency = 50,
        tbuff = 1714,
        duration = 30,
    },
    ["Glower"] = {
        potency = 220,
        falloff = true,
        tbuff = 17,
        duration = 6,
        aoetype = BLUE.AOETYPE.LINE
    },
    ["Plaincracker"] = {
        potency = 220,
        falloff = true,
        aoetype = BLUE.AOETYPE.SAOE
    },
    ["Bristle"] = {
        sbuff = 1716,
        duration = 30
    },
    ["White Wind"] = {
        aoetype = BLUE.AOETYPE.SAOE
    },
    ["Sharpened Knife"] = {
        potency = 220,
        modifier = {
            condition = function()
                return HasBuff(Player:GetTarget(), 0)
            end,
            potency = 450
        },
    },
    ["Ice Spikes"] = {},
    ["Blood Drain"] = {
        potency = 50,
    },
    ["Acorn Bomb"] = {
        tbuff = 3,
        duration = 30,
        aoetype = BLUE.AOETYPE.TAOE,
    },
    ["Bomb Toss"] = {
        potency = 200,
        falloff = true,
        tbuff = 0,
        duration = 3,
        aeotype = BLUE.AOETYPE.GAOE,
    },
    ["Off-guard"] = {
        tbuff = 1717,
        duration = 15,
    },
    ["Self-destruct"] = {
        potency = 1500,
        modifier = {
            condition = function()
                return HasBuff(Player:GetTarget(), 1737)
            end,
            potency = 1800
        },
        suicide = true,
        aoetype = BLUE.AOETYPE.SAOE,
    },
    ["Transfusion"] = {
        suicide = true,
    },
    ["Faze"] = {
        tbuff = 0,
        duration = 6,
        aoetype = BLUE.AOETYPE.CONE,
    },
    ["Flying Sardine"] = {
        potency = 10,
        interrupts = true,
    },
    ["The Look"] = {
        potency = 220,
        falloff = true,
        aoetype = BLUE.AOETYPE.CONE,
    },
    ["Bad Breath"] = {
        dpotency = 20,
        duration = 15,
        tbuff = 1715, -- 18, 14, 9, 15, 17, -- Malodorous, Poison, Heavy, Slow, Blind, Paralysis
        interrupts = true,
        aoetype = BLUE.AOETYPE.CONE
    },
    ["Diamondback"] = {
        sbuff = 1722,
        duration = 10,
    },
    ["Mighty Guard"] = {
        sbuff = 1719,
    },
    ["Sticky Tongue"] = {
        tbuff = 0,
        duration = 4
    },
    ["Toad Oil"] = {
        sbuff = 1737,
        duration = 180,
    },
    ["The Ram's Voice"] = {
        potency = 220,
        falloff = true,
        duration = 12,
        tbuff = 1731,
        aoetype = BLUE.AOETYPE.SAOE,
    },
    ["The Dragon's Voice"] = {
        potency = 200,
        falloff = true,
        duration = 9,
        tbuff = 17,
        modifier = {
            condition = function()
                return HasBuff(Player:GetTarget(), 1731)
            end,
            potency = 400
        },
        mindistance = 8,
        aoetype = BLUE.AOETYPE.DONUT,
    },
    ["Thousand Needles"] = {
        aoetype = BLUE.AOETYPE.SAOE,
    },
    ["Tail Screw"] = {},
    ["Mind Blast"] = {
        potency = 200,
        falloff = true,
        tbuff = 17,
        duration = 30,
        BLUE.AOETYPE.SAOE,
    },
    ["Peculiar Light"] = {
        tbuff = 1721,
        duration = 15,
        BLUE.AOETYPE.SAOE,
    },
    ["Feather Rain"] = {
        potency = 220,
        falloff = false,
        tbuff = 1723,
        dpotency = 40,
        duration = 6,
        aeotype = BLUE.AOETYPE.GAOE,
    },
    ["Shock Strike"] = {
        potency = 400,
        falloff = true,
        aeotype = BLUE.AOETYPE.TAOE,
    },
    ["Glass Dance"] = {
        potency = 350,
        falloff = true,
        aoetype = BLUE.AOETYPE.HALFCIRCLE
    },
    ["Veil of the Whorl"] = {
    },
    ["Alpine Draft"] = {
        potency = 220,
        falloff = true,
    },
    ["Protean Wave"] = {
        potency = 220,
        falloff = true,
        range = 15,
    },
    ["Northerlies"] = {
        potency = 220,
        falloff = true,
        modifier = {
            condition = function() end,
            tbuff = 'frozen',
        },
    },
    ["Electrogenesis"] = {
        potency = 220,
        falloff = true,
    },
    ["Sonic Boom"] = {
        potency = 210
    },
    ["Revenge Blast"] = {
        potency = 50,
        modifier = {
            condition = function() end,
            potency = 500
        },
    },
    ["Devour"] = {
        potency = 250,
        duration = 15,
        sbuff = 2120, -- HP Boost
        modifier = {
            condition = BLUE.IsTank,
            potency = 250,
            duration = 70,
            sbuff = 2120, -- HP Boost
        }
    },
    ["Exuviation"] = {
        cpotency = 50,
        esuna = true,
        modifier = {
            condition = BLUE.IsHealer,
            cpotency = 300,
            esuna = true,
        }
    },
    ["Triple Trident"] = {
        potency = 150,
        hits = 3,
    },
    ["Blaze"] = {
        potency = 220,
        falloff = true,
    },
    ["Mustard Bomb"] = {
        potency = 220,
        modifier = {
            condition = function() end,
            dpotency = 50,
            duration = 15,
        },
    },
    ["Aetherial Spark"] = {
        potency = 50,
        dpotency = 50,
        duration = 15,
    },
    ["Choco Meteor"] = {
        potency = 200,
        falloff = true,
        modifier = {
            condition = function()
                return table.randomvalue(EntityList("contentid=952,ownerid="..Player.id))
            end, 
            potency = 300,
            falloff = true,
        }
    },
    ["Peripheral Synthesis"] = {
        potency = 220,
        falloff = true,
        tbuff = 0,
        modifier = {
            condition = function() end,
            potency = 400,
        },
    },
    ["Nightbloom"] = {
        potency = 400,
        dpotency = 75,
        duration = 60,
        falloff = true,
        tbuff = 1714,
    },
}

-- -------------------------------------------------------------------------- --
-- ANCHOR                    Registration and Return                          --
-- -------------------------------------------------------------------------- --

return BLUE